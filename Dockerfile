from ros:noetic


RUN apt-get update && apt install -y python3-pip\
    && pip3 install --upgrade pip \
    && pip3 install -U numpy \
    && pip3 install -U pybullet

# Install ROS extra packages
RUN apt-get update &&\ 
    apt-get install -y python3-rosdep \
        python3-rosinstall \
        python3-rosinstall-generator \
        python3-wstool \
        python3-catkin-tools \
        python3-lxml \
        build-essential \
    && rm -rf /var/lib/apt/lists/* && apt autoremove && apt clean

# Install ROS packages
RUN apt-get update \
    && apt-get install -y ros-${ROS_DISTRO}-ros-control \ 
        ros-${ROS_DISTRO}-joint-state-controller \
        ros-${ROS_DISTRO}-velocity-controllers \
        ros-${ROS_DISTRO}-effort-controllers \ 
        ros-${ROS_DISTRO}-position-controllers \ 
        ros-${ROS_DISTRO}-robot-state-publisher \
        ros-${ROS_DISTRO}-teleop-twist-keyboard \
    && rm -rf /var/lib/apt/lists/* && apt autoremove && apt clean

RUN apt-get update \
    && apt-get install -y ros-${ROS_DISTRO}-ur-robot-driver \
        ros-${ROS_DISTRO}-rqt-joint-trajectory-controller \ 
        ros-${ROS_DISTRO}-universal-robots \ 
        ros-${ROS_DISTRO}-moveit \ 
    && rm -rf /var/lib/apt/lists/* && apt autoremove && apt clean

RUN apt-get update \
    && apt-get install -y ros-${ROS_DISTRO}-realsense2-description \
        ros-${ROS_DISTRO}-realsense2-camera \
    && rm -rf /var/lib/apt/lists/* && apt autoremove && apt clean



RUN mkdir -p /ros_ws/src && cd /ros_ws \ 
    && catkin config --extend /opt/ros/${ROS_DISTRO} \
    && catkin build && echo "source /ros_ws/devel/setup.bash" >> ~/.bashrc

RUN pip3 install pyserial
WORKDIR /ros_ws

COPY ./ /ros_ws/src/ur_cell

RUN catkin build

HEALTHCHECK --interval=20s --timeout=1s --retries=3 --start-period=20s CMD if [[ $(rostopic list 2> /dev/null | wc -c) > 0 ]]; then exit 0; fi;
CMD ["/bin/bash", "-ci", "roslaunch referee_box bringup.launch"]

